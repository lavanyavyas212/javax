
/**
 * Write a description of class Sample here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
import java.io.*;
public class Sample
{
   public static void main(String[]args) throws IOException{
       BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
       System.out.println("Input Employee Code");
       int emp = Integer.parseInt(br.readLine());
       System.out.println("Input Salary");
       double sal = Double.parseDouble(br.readLine());
       double allow = (17.0/100.0)*sal;
       double total = sal + allow;
       System.out.println("Employee code "+emp);
       System.out.println("Salary "+sal);
       System.out.println("Allowance "+allow);
       System.out.println("Total "+total);
}
}
