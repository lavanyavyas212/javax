package ch05;

import java.util.*;
public class P02{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter price ");
        double price = sc.nextDouble();
        
        System.out.println("Enter quantity ");
        int qty = sc.nextInt();
        
        double total = price*qty;
        double disc;
        if(total>=18000)
        {
             disc = (total*30.0)/100.0;
        }
        else 
        {
            disc = (total*10.0)/100.0;
        }
        
        double netprice = total - disc;
        System.out.println("Price "+price);
        System.out.println("Quantity "+qty);
        System.out.println("Total "+total);
        System.out.println("Discount "+disc);
        System.out.println("Netprice "+netprice);
    }
}