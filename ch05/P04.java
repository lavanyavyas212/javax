package ch05;

import java.util.*;
public class P04{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter first integer ");
        int a = sc.nextInt();
        System.out.println("Enter second integer ");
        int b = sc.nextInt();
        System.out.println("Enter arithmetic operator: +,-,*,/ ");
        char ch = sc.next().charAt(0);
        int result;
        if(ch=='+')
        {
            result = a+b;
            System.out.println("Addition of "+a+" and "+b+" = "+result);
        }
        else if(ch=='-')
        {
            result = a-b;
            System.out.println("Subtraction of "+a+" and "+b+" = "+result);
        }
        else if(ch=='*')
        {
            result = a*b;
            System.out.println("Multiplication of "+a+" and "+b+" = "+result);
        }
        if(ch=='/')
        {
            result = a/b;
            System.out.println("Division of "+a+" and "+b+" = "+result);
        }
    }
}