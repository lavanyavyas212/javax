package ch05;

import java.util.*;
public class P12{
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter first number");
        int n1 = sc.nextInt();
        System.out.println("Enter second number");
        int n2 = sc.nextInt();
        System.out.println("Enter operators: +,-,/,* ");
        char opr = sc.next().charAt(0);
        if(opr=='+')
        {
            int sum = n1 + n2;
           System.out.println("Sum of "+n1+" and "+n2+" = "+sum);
        }
        else if(opr=='-')
        {
            if(n1>n2)
           {
               int difference = n1 - n2;
               System.out.println("Difference of "+n1+" and "+n2+" = "+difference);
            }
            else 
            {
                int difference = n2 - n1;
               System.out.println("Difference of "+n2+" and "+n1+" = "+difference);
            }
        }
        else if(opr=='*')
        {
            int product = n1 * n2;
           System.out.println("Product of "+n1+" and "+n2+" = "+product);
        }
            else if(opr=='/')
        {
            if(n1>n2)
           {
               int Division = n1 / n2;
               System.out.println("Division of "+n1+" and "+n2+" = "+Division);
            }
            else 
            {
                int Division = n2 / n1;
               System.out.println("Difference of "+n2+" and "+n1+" = "+Division);
            }
        }
    }
}