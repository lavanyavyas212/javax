package ch05;

import java.util.*;
public class P01{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a character");
        char ch = sc.next().charAt(0);
        if(Character.isUpperCase(ch))
        {
            System.out.println(ch+" is in uppercase");
        }
        else if(Character.isLowerCase(ch))
        {
            System.out.println(ch+ " is in lowercase");
        }
        else if(Character.isDigit(ch))
        {
            System.out.println(ch+" is a digit");
        }
        else
        {
            System.out.println(ch+" is neither an alphbet or digit");
        }
    }
}
        