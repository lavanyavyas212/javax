package ch05;

import java.util.*;
class P10{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter total marks");
        int marks = sc.nextInt();
        if(marks>=300)
        {
            System.out.println("Stream offered Science");
        }
        else if(marks>=200 && marks <300)
        {
            System.out.println("Stream offered Commerce");
        }
        
        else if(marks>=75 && marks <200)
        {
            System.out.println("Stream offered Arts");
        }
        else
        {
            System.out.println("Admission is not granted , you have to appear in a qualifying examination");
        }
    }
}
        