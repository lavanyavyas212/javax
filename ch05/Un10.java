package ch05;

import java.util.*;
public class Un10{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter no.of units");
        int unit = sc.nextInt();
        double total ;
        if(unit<=100)
        {
            total = 200.00;
            
            System.out.println("Units:  "+unit);
            System.out.println("Amount:  "+total);
        }
        else if(unit>100 && unit<=200)
        {
            total = (unit-100)*1.0 + 200.0;
            System.out.println("Units:  "+unit);
            System.out.println("Amount:  "+total);
        }
        else if(unit>=200 && unit<=500)
        {
            total = (unit-200)*1.55+(100*1.00) + 200.0;
            System.out.println("Units:  "+unit);
            System.out.println("Amount:  "+total);
        }
        else if(unit>=500)
        {
            total = (unit-500)*2.10 +(200*1.55)+(100*1.00) + 200.0;
            System.out.println("Units:  "+unit);
            System.out.println("Amount:  "+total);
        }
    }
}