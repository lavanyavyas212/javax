package ch05;

public class P08{
    public static void main(String[] args){
        int a = 20;
        int b = 13;
        int c = 10;
        int x = (a>b)?a:b;
        int y = (x>c)?x:c;
        System.out.println("Greatest number "+y);
        
        int n1= 34;
        int n2 = 45;
        int n3  = 78;
        int z = (n1>n2)?((n1>n3)?n1:n3):((n2>n3)?n2:n3);
        System.out.println("Greatest number "+z);
    }
} 