package ch05;

import java.util.*;
public class S3{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Menu");
        System.out.println("Add--->A or a");
        System.out.println("Subtract---> S or s");
        System.out.println("Multiply---> M or m");
        System.out.println("Divide---> D or d");
        char ch = sc.next().charAt(0);
        switch(ch)
        {
            case 'A' :
            case 'a':
            System.out.println("Enter first number");
            int num1 = sc.nextInt();
            System.out.println("Enter second number");
            int num2 = sc.nextInt();
            int result1  = num1 + num2;
            System.out.println(num1+" + "+num2+" = "+result1);
            break;


            case 'S' :
            case 's':
            System.out.println("Enter first number");
            int num3 = sc.nextInt();
            System.out.println("Enter second number");
            int num4 = sc.nextInt();
            int result2  = num3 - num4;
            System.out.println(num3+" - "+num4+" = "+result2);
            break;    
            
            
            case 'M' :
            case 'm':
            System.out.println("Enter first number");
            int num5 = sc.nextInt();
            System.out.println("Enter second number");
            int num6 = sc.nextInt();
            int result3  = num5 * num6;
            System.out.println(num5+" * "+num6+" = "+result3);
            break;
            
            
            case 'D' :
            case 'd':
            System.out.println("Enter first number");
            int num7 = sc.nextInt();
            System.out.println("Enter second number");
            int num8 = sc.nextInt();
            int result4  = num7 / num8;
            System.out.println(num7+" / "+num8+" = "+result4);
            break;
            
            default:
            System.out.println("Wrong choice");
            break;
        }
    }
}