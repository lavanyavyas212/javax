package ch05;

import java.util.*;
public class S1_Menu{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("MENU");
        System.out.println("1---PRINT ABSOLUTE VALUE");
        System.out.println("2---PRINT ROUNDED UP VALUE");
        System.out.println("3---PRINT NUMBER RAISED TO A POWER");
        System.out.println("4---PRINT EXIT");
        
        int choice = sc.nextInt();
        switch(choice)
        {
            case 1: 
            System.out.println("Enter a number");
            double num = sc.nextDouble();
            double ab = Math.abs(num);
            System.out.println("Absolute value: "+ab);
            break;
            
            case 2:
            System.out.println("Enter a number");
            double num1 = sc.nextDouble();
            double c = Math.ceil(num1);
            System.out.println("Rounded up value: "+c);
            break;
            
            
            case 3:
            System.out.println("Enter a number");
            double n = sc.nextDouble();
            System.out.println("Enter its power");
            double x = sc.nextDouble();
            double p = Math.pow(n,x);
            System.out.println(n+" raised to "+x+" = "+p);
            break;
            
            case 4:
            System.exit(0);
            
            default:
            System.out.println("Wrong choice");
            break;
        }
    }
}