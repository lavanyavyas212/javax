package ch05;

import java.util.*;
public class S2{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("MENU");
        System.out.println("1--Fahrenheit to celsius");
        System.out.println("2--Celsius to fahrenheit");
        int choice = sc.nextInt();
        switch(choice)
        {
            case 1:
            System.out.println("Enter temperature in fahrenheit");
            double f = sc.nextDouble();
            double c = (5.0/9.0)*(f-32);
            System.out.println(c+" degree celsius");
            break;
            
            case 2:
            System.out.println("Enter temperature in celsius");
            double c1 = sc.nextDouble();
            double f1 = 1.8*c1+32;
            System.out.println(f1+" degree fahrenheit");
            break;
            
            default:
            System.out.println("Wrong choice");
            break;
        }
    }
}