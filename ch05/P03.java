package ch05;

import java.util.*;
public class P03{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Input a character ");
        char ch = sc.next().charAt(0);
        System.out.println("Original character "+ch);
        char ch1;
        if(Character.isUpperCase(ch))
        {
            ch = Character.toLowerCase(ch);
            System.out.println("New character "+ch);
        }
        else if(Character.isLowerCase(ch))
        {
            ch = Character.toUpperCase(ch);
            System.out.println("New character "+ch);
        }
        
        
    }
}