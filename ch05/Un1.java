package ch05;

import java.util.*;
public class Un1{
    public static void main(String[]args){
    Scanner sc = new Scanner(System.in);
    System.out.println("1--Logarithm");
    System.out.println("2--Absolute");
    System.out.println("3--Square root");
    System.out.println("4--Random no. between 0-1");       
    
    int choice = sc.nextInt(); 
    switch(choice)
    {
        case 1:
        System.out.println("Enter a number");
        int n = sc.nextInt();
        double s = Math.log(n);
        System.out.println("Answer "+s);
        break;
        
        case 2:
        System.out.println("Enter a number");
         n = sc.nextInt();
        double t = Math.abs(n);
        System.out.println("Answer "+t);
        break;
        
        
        case 3:
        System.out.println("Enter a number");
         n = sc.nextInt();
        double u = Math.sqrt(n);
        System.out.println("Answer "+u);
        break;
        
        
        case 4:
        System.out.println("Answer "+Math.random());
       break;
        
        default:
        System.out.println("Wrong choice");
    }
}
}
    
