package ch04;






/**
 * Write a description of class Student here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Student
{
    // instance variables - replace the example below with your own
    private int roll;
    private  String name;
    private int age;
    private String grd;
    private String house;

    /**
     * Constructor for objects of class Student
     */
    public Student()
    {
        // initialise instance variables
        
    }
    
    public Student(int roll , String name , int age,String grd,String house){
        this.roll  = roll;
        this.name = name;
        this.age = age;
        this.grd = grd;
        this.house = house;
        
    }
    
    
    public String toString(){
     String str = "roll : " + roll + "\t name : "+name+ "\t age: "+age+" \t Grade:  "+grd+" \t House: "+house ;    
        return str;
    }
    
     int getRoll()
    {
     return roll;   
    }
    
    public String getName(){
    
    return name;
   
    }
    
    public int getAge(){
        
        return age;
        
    }
    
    public void setAge(int agenew ){
        this.age = agenew;
    }
}

