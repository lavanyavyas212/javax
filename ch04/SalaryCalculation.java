package ch04;

public class SalaryCalculation{
    String name;
    double basicPay ,specialAlw , conveyanceAlw , gross , pf,netSalary, AnnualSal;
    
  public void giveValues()
  {
     name = "K";
     basicPay = 100.00;
     conveyanceAlw = 1000.00;
  }
  public void SalaryCal()
  {
      specialAlw = (25.0/100.0)*basicPay;
      gross = basicPay + specialAlw + conveyanceAlw;
      pf = (8.33/100.0)*basicPay;
      netSalary = gross - pf;
      AnnualSal = netSalary*12;
  }
  
  public void display()
  {
      System.out.println("Name: "+name);
      System.out.println("Basic salary "+basicPay);
      System.out.println("Conveyance allowance "+conveyanceAlw);
      System.out.println("Special allowance "+specialAlw);
      System.out.println("Gross "+gross);
      System.out.println("Provident fund "+pf);
      System.out.println("Net salary "+netSalary);
      System.out.println("Annual salary "+AnnualSal);
  }
  public static void main(String[] args)
  {
      SalaryCalculation sal = new SalaryCalculation();
      sal.giveValues();
      sal.SalaryCal();
      sal.display();
    }
}