package ch04;

public class PrintMessage{
    public void message()
    {
        System.out.println("I love Chandler");
        System.out.println("I am a food lover");
        System.out.println("I love self-appreciating myself");
    }
    public void print()
    {
        PrintMessage ob = new PrintMessage();
        ob.message();
    }
    public static void main(String[] args)
    {
        PrintMessage p = new PrintMessage();
        p.print();
    }
}        