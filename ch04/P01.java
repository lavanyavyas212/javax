package ch04;
import java.util.*;
public class P01{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a character ?");
        char ch = sc.next().charAt(0);
        if(Character.isDigit(ch))
        {
            System.out.println(ch+" is a digit");
        }
        else
        {
            System.out.println(ch+" is not a digit");
        }
        if(Character.isWhitespace(ch))
        {
            System.out.println(ch+" + is a space character");
        }
        else
        {
            System.out.println(ch+" is not a space character");
        }
    }
}