import java.util.*;
public class Employee{
    public static void main(String[]args){
        double basic;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your basic pay");
        basic = sc.nextDouble();
        double DA = (25.0/100.0)*basic;
        System.out.println("Your DA:  "+DA);
        double HRA = (15.0/100.0)*basic;
        System.out.println("Your HRA:  "+HRA);
        double PF = (8.33/100.0)*basic;
        System.out.println("Your PF:  "+PF);
        double Net = basic + DA + HRA;
        System.out.println("Your Net pay:  "+Net);
        double Gross = Net - PF;
        
        System.out.println("Your gross pay:  "+Gross);
    }
}