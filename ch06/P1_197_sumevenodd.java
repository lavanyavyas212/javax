package ch06;


/**
 * Write a description of class P1_197_sumevenodd here.
 * to find sum of even and odd in a given no.
 * @author (your name)
 * @version (a version number or a date)
 */
public class P1_197_sumevenodd
{ 
    public static void main(String[] args){
      int n = 10;
      int even = 0; 
      int odd = 0;
      int sum = 0 ;
      for(int i = 1 ; i<=n ; i++){
          if(i%2==0){
          even = even + i;
        }
        else{
            odd = odd + i;
        }
        sum = sum +i;
    }
    System.out.println("even = "+even);
    System.out.println("odd  ="+odd);
    System.out.println("sum =  "+sum);
  }
}