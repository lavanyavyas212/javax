package ch06;


/**
 * Write a description of class Factorial here.
 * to check whether no.is factorial or not 
 * eg (4!) = 1*2*3*4 = 24; 
 * @author (Lavanya vyas)
 * @version (v1.1)
 */
public class Factorial
{
    // instance variables - replace the example below with your own
    private int x;

    /**
     * Constructor for objects of class Factorial
     */
    public Factorial()
    {
        // initialise instance variables
        x = 0;
    }

    /**
     * An example of a method - replace this comment with your own
     *
     * @param  y  a sample parameter for a method
     * @return    the sum of x and y
     */
    public int sampleMethod(int y)
    {
        // put your code here
        return x + y;
    }
}
