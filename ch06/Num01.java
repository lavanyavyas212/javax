package ch06;

class Num01{
    public static void main(String[] args){
      
        int space = 30, n = 11;
        for(int i = 0 ; i<n ;i++){
           for(int j = 1 ; j<space-i ; j++)
           {
            System.out.print(" ");
           }
           for(int k = i-1 ;k>=0;k--)
           {
             System.out.print(k); 
           }
           for(int l = 0 ; l<i ; l++)
           {
            if(l==0)
            {
             continue;    
            }
            System.out.print(l);
           }
           System.out.println();
        }
    }
}
 