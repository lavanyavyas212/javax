package ch06;


/**
 * Write a description of class Numpattern02 here.
 * 1 
 * 2 2
 * 3 3 3
 * 4 4 4 4
 * 5 5 5 5 5
 * @author (your name)
 * @version (a version number or a date)
 */
public class Numpattern02
{

    public static void main(String[] args){
    
        int n = 5;
        for(int i = 1 ; i<=n ; i++){
           for(int j = 1; j<=i ; j++){
               System.out.print(i+" ");
            }
            System.out.println(); 
        } 
    }
}