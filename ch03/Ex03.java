package ch03;


/**
 * Write a description of class Ex03 here.
 * Program code to input a character
 *
 * @author (Lava)
 * @version (1.0)
 */
import java.io.*;
public class Ex03
{
    public static void main(String[] args) throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Input a character");
        String ab = br.readLine();
        System.out.println("1. You typed "+ab);
        
        System.out.println(" Again Input a character");
        char ch =(char)System.in.read();
        System.out.println("2. You typed again  "+ch);
        
        
        
    }
    
}
