
/**
 * program to input an int and print it.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
import java.io.*;
public class Ex01
{
    public static void main(String[] args) throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("1.Enter a number");
        int n = Integer.parseInt(br.readLine());
        System.out.println("1.the integer is  "+n);
        
        System.out.println("2. Enter a number");
        double y = Double.parseDouble(br.readLine());
        System.out.println("2.the double no is "+y);
        
        System.out.println("3.Enter a number");
         float f = Float.parseFloat(br.readLine());
        System.out.println("3.the float is "+f);
        
        
    }
}
