
/**
 * Write a description of class Student here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Student{
 int roll;
 String name;
 String std;
 String house;   
    
    public Student(int roll , String name, String std , String house){
    this.roll = roll;
    this.name = name;
    this.std  = std;
    this.house = house;
 }
 public static void main(String[] args){
     Student st1 = new Student(12,"Samridhi Vyas","VII","Stark");
     Student st2 = new Student(78,"Lavanya Vyas","X","Lannister");
     Student st3 = new Student(67,"Anand Vyas","XII","Targaryen"); 
     Student st4 = new Student(92,"Vaishali Vyas","V","Baratheon");    
     Student st5 = new Student(108,"Anaisha Kapoor","VIII","Tyrell");  
     Student st6 = new Student( 2,"Warren Buffay","I","Opal");
     Student st7 = new Student(78,"Bill Gates","X","Topaz");
     Student st8 = new Student(6,"Jeff Bezos","XV","Ruby"); 
     Student st9 = new Student(9,"Jack Ma","III","Poseidon");    
     Student st10 = new Student(10,"Chandler Bing","II","Aquamarine");        
    }
}
