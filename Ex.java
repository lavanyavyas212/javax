import java.util.*;
public class Ex{
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        double salary;
        System.out.println("Input salary");
        if(sc.hasNextInt())
        {
            salary = (double)sc.nextInt();
            System.out.println("salary "+salary);
        }
        else if(sc.hasNextDouble())
        {
            salary = sc.nextDouble();
            System.out.println("salary "+salary);
        }
        else if(sc.hasNextFloat())
        {
            salary = (double)sc.nextFloat();
            System.out.println("salary "+salary);
        }
        else
        {
            System.out.println("The salary is not an integer or a fractional value");
        }
    }
}