package ch07;

import java.util.*;
public class Distance{
    public static double distanc(double u , double time , double acc){
        double d = (u*time)+(0.5*acc*(time*time));
        return d;
    }
    public  static void main(String[]args){
        double v;
        double t;
        double a ;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter velocity");
        v = sc.nextDouble();
        System.out.println("Enter time");
        t = sc.nextDouble();
        System.out.println("Enter acceleration");
        a = sc.nextDouble();
        Distance dd = new Distance();
        double result = distanc(v,t,a);
        
        System.out.println("Velocity "+v);
        System.out.println("Time "+t);
        System.out.println("Acceleration "+a);
         System.out.println("Distance "+result);
         
    }
}
    
   