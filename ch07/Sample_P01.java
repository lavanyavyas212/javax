package ch07;

public class Sample_P01{
    public void product(int code , float price , float qty )
    {
        double total = price*qty;
        double disc = (12.0/100.0)*total;
        double net  = total - disc;
        
        System.out.println("Code: "+code);
        System.out.println("Price: "+price);
        System.out.println("Quantity: "+qty);
        System.out.println("Total: "+total);
        System.out.println("Discount: "+disc);
        System.out.println("Net price: "+net);
        
    }
    public void run()
    {
        product(0045,13500,12);
    }
    public static void main(String[] args)
    {
       Sample_P01 s = new Sample_P01();
       
       s.run();
}
}